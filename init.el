;; Set up emacs packages

(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives
	     '("melpa-stable" . "http://stable.melpa.org/packages/"))
(setq package-archive-priorities
      '(("gnu" . 2) ("melpa-stable" . 1)))

(setq package-enable-at-startup nil)
(package-initialize)

;; Make sure use-package is installed before setting up other packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))

(setq use-package-always-ensure t)
;; (setq use-package-always-pin "melpa-stable")

;; Install/initialize/configure packages with use-package
(use-package evil
  :init
  ;; evil plugins
  (use-package evil-visualstar :config (global-evil-visualstar-mode))
  (use-package evil-surround :config (global-evil-surround-mode))
  (use-package evil-args
    :config
    ;; bind evil-args text objects
    (define-key evil-inner-text-objects-map "a" 'evil-inner-arg)
    (define-key evil-outer-text-objects-map "a" 'evil-outer-arg)
    ;; bind evil-forward/backward-args
    (define-key evil-normal-state-map "L" 'evil-forward-arg)
    (define-key evil-normal-state-map "H" 'evil-backward-arg)
    (define-key evil-motion-state-map "L" 'evil-forward-arg)
    (define-key evil-motion-state-map "H" 'evil-backward-arg))
  :config
  (evil-mode 1))

(use-package helm
  :config
  (require 'helm-config)
  (global-set-key (kbd "M-x") #'helm-M-x)
  (global-set-key (kbd "C-x C-f") #'helm-find-files)
  (global-set-key (kbd "C-x b") #'helm-buffers-list)
  (global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
  (helm-mode 1))

(use-package company
  :init
  (use-package company-math)
  :config (global-company-mode))

(use-package adaptive-wrap)

(use-package yasnippet)

;; (use-package powerline
;;   :config (powerline-default-theme))

(use-package magit)

(use-package tex
  :ensure auctex
  :init
  (use-package auctex-latexmk
    :init (setq auctex-latexmk-inherit-TeX-PDF-mode t)
    :config (auctex-latexmk-setup))
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq reftex-plug-into-auctex t)
  :config
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'adaptive-wrap-prefix-mode)
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode))

(use-package anaconda-mode
  :init
  (use-package company-anaconda
    :config
    (add-to-list 'company-backends '(company-anaconda :with company-capf)))
  :config
  (add-hook 'python-mode-hook 'anaconda-mode))


(add-hook 'text-mode-hook 'linum-mode)
(add-hook 'prog-mode-hook 'linum-mode)
(column-number-mode)
(show-paren-mode)

(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

(define-key evil-motion-state-map "k" 'evil-previous-visual-line)
(define-key evil-motion-state-map "j" 'evil-next-visual-line)


;; ------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (adwaita)))
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "5b340b4eb24c3c006972f3bc3aee26b7068f89ba9580d9a4211c1db5d0a70ea2" default)))
 '(font-use-system-font t)
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount (quote (3 ((shift) . 1) ((control)))))
 '(package-selected-packages
   (quote
    (solarized-theme badwolf-theme yasnippet use-package magit helm evil company auctex adaptive-wrap))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :slant normal :weight normal :height 98 :width normal)))))
